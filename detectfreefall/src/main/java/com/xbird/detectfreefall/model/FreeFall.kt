package com.xbird.detectfreefall.model

import androidx.room.Entity
import androidx.room.PrimaryKey
import java.util.*

@Entity
data class FreeFall(
    @PrimaryKey(autoGenerate = true) val id: Int = 0,
    val startTime: Date,
    val endTime: Date,
    val shock: Double
    ) {
    fun getFallDuration() = (endTime.time - startTime.time)
}