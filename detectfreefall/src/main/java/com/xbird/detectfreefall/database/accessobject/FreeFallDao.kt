package com.xbird.detectfreefall.database.accessobject

import androidx.lifecycle.LiveData
import androidx.room.Dao
import androidx.room.Insert
import androidx.room.Query
import com.xbird.detectfreefall.model.FreeFall

@Dao
internal interface FreeFallDao {
    @Query("SELECT * FROM freefall ORDER by startTime DESC")
    fun getAllFalls(): LiveData<List<FreeFall>>

    @Insert
    suspend fun insertFall(freeFall: FreeFall)
}