package com.xbird.detectfreefall.database

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import androidx.room.TypeConverters
import com.xbird.detectfreefall.access.FallAccess
import com.xbird.detectfreefall.database.accessobject.FreeFallDao
import com.xbird.detectfreefall.model.FreeFall

@Database(entities = [FreeFall::class], version = 1)
@TypeConverters(RoomTypeConverter::class)
internal abstract class AppDatabase : RoomDatabase() {
    abstract fun freeFallDao() : FreeFallDao
    internal fun getFallAccess(): FallAccess = FallAccess(freeFallDao())

    companion object {
        @Volatile
        private var INSTANCE: AppDatabase? = null

        fun getDatabase(context: Context): AppDatabase {
            val tempInstance = INSTANCE
            if (tempInstance != null) {
                return tempInstance
            }
            synchronized(this) {
                val instance = Room.databaseBuilder(
                    context.applicationContext,
                    AppDatabase::class.java,
                    "free_fall_database"
                ).build()
                INSTANCE = instance
                return instance
            }
        }
    }
}