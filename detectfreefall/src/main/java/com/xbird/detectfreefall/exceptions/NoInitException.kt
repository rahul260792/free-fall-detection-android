package com.xbird.detectfreefall.exceptions

class NoInitException: Exception("Please initialize the library first")