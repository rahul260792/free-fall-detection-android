package com.xbird.detectfreefall.exceptions

class AccelerometerUnavailableException : Exception("Accelerometer Unavailable. Can't detect the fall")