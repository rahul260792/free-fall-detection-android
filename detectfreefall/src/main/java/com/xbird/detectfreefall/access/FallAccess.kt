package com.xbird.detectfreefall.access

import com.xbird.detectfreefall.database.accessobject.FreeFallDao
import com.xbird.detectfreefall.model.FreeFall

internal class FallAccess(private val freeFallDao: FreeFallDao) {
    val allFreeFalls = freeFallDao.getAllFalls()

    suspend fun insert(freeFall: FreeFall){
        freeFallDao.insertFall(freeFall)
    }
}