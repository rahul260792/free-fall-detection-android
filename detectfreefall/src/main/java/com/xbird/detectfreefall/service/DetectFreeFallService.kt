package com.xbird.detectfreefall.service

import android.app.Notification
import android.app.NotificationChannel
import android.app.NotificationManager
import android.app.Service
import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorEvent
import android.hardware.SensorEventListener
import android.hardware.SensorManager
import android.os.Build
import android.os.IBinder
import androidx.annotation.RequiresApi
import androidx.core.app.NotificationCompat
import com.xbird.detectfreefall.DetectFreeFall
import com.xbird.detectfreefall.R
import com.xbird.detectfreefall.access.FallAccess
import com.xbird.detectfreefall.database.AppDatabase
import com.xbird.detectfreefall.exceptions.AccelerometerUnavailableException
import com.xbird.detectfreefall.model.FreeFall
import java.util.*
import kotlin.math.pow
import kotlin.math.sqrt
import kotlinx.coroutines.*

internal class DetectFreeFallService : Service(), SensorEventListener, CoroutineScope by MainScope() {

    companion object {
        const val DEFAULT_SENSOR_DELAY = SensorManager.SENSOR_DELAY_UI
        const val CHANNEL_DETECT_FALL_RUNNING = "channel_detect_fall_running"
        const val CHANNEL_FALL_DETECTED = "channel_fall_detected"
        const val EXTRA_KEY_SHOCK = "DetectFreeFallService.EXTRA_KEY_SHOCK"
        const val EXTRA_KEY_SENSOR_DELAY = "DetectFreeFallService.EXTRA_KEY_SENSOR_DELAY"
        const val FOREGROUND_ID = 188526
        const val DEFAULT_FREE_FALL_SHOCK = 2.0
    }

    private lateinit var sensorManager: SensorManager
    private lateinit var notificationManager: NotificationManager
    private lateinit var fallAccess: FallAccess
    private var accelerometerSensor: Sensor? = null
    private var isInFall = false
    private var fallStartTime = Calendar.getInstance().time
    private var sensorDelay = DEFAULT_SENSOR_DELAY
    private var threshold = DEFAULT_FREE_FALL_SHOCK

    override fun onCreate() {
        super.onCreate()
        notificationManager = getSystemService(Context.NOTIFICATION_SERVICE) as NotificationManager
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
            createNotificationChannel(
                CHANNEL_DETECT_FALL_RUNNING,
                getString(R.string.detector_servcie),
                getString(R.string.detector_service_description),
                NotificationManager.IMPORTANCE_DEFAULT
            )

            createNotificationChannel(
                CHANNEL_FALL_DETECTED,
                getString(R.string.fall_detected),
                getString(R.string.fall_detected_description),
                NotificationManager.IMPORTANCE_DEFAULT
            )
        }
        sensorManager = getSystemService(Context.SENSOR_SERVICE) as SensorManager
        accelerometerSensor = sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER)
        if (accelerometerSensor == null) {
            throw AccelerometerUnavailableException()
        }
        fallAccess = AppDatabase.getDatabase(this).getFallAccess()
    }

    @RequiresApi(Build.VERSION_CODES.O)
    private fun createNotificationChannel(
        channelId: String, channelName: String,
        channelDescription: String, importance: Int
    ) {
        val serviceChannel = NotificationChannel(
            channelId,
            channelName,
            importance
        ).apply { description = channelDescription }

        notificationManager.createNotificationChannel(serviceChannel)
    }

    override fun onBind(p0: Intent?): IBinder? {
        return null
    }

    override fun onStartCommand(intent: Intent, flags: Int, startId: Int): Int {

        accelerometerSensor?.let {
            isInFall = false
            threshold = intent.getDoubleExtra(EXTRA_KEY_SHOCK, DEFAULT_FREE_FALL_SHOCK)
            sensorDelay = intent.getIntExtra(EXTRA_KEY_SENSOR_DELAY, DEFAULT_SENSOR_DELAY)
            // Unregister old listener if any
            unregisterListener()
            sensorManager.registerListener(this, it, sensorDelay)
            DetectFreeFall.setIsFallDetectionRunning(true)
            val notification: Notification = NotificationCompat.Builder(
                this,
                CHANNEL_DETECT_FALL_RUNNING
            )
                .setContentTitle(getString(R.string.fall_detection_running_notification_title))
                .setContentText(getString(R.string.fall_detection_running_notification_body))
                .setSmallIcon(R.drawable.ic_fall)
                .build()
            startForeground(FOREGROUND_ID, notification)
        }

        return START_STICKY
    }

    private fun unregisterListener() {
        accelerometerSensor?.let {
            sensorManager.unregisterListener(this, it)
        }
        DetectFreeFall.setIsFallDetectionRunning(false)
    }

    override fun onDestroy() {
        unregisterListener()
        stopForeground(true)
        cancel()
        super.onDestroy()
    }

    override fun onAccuracyChanged(p0: Sensor?, p1: Int) {
    }

    override fun onSensorChanged(event: SensorEvent) {
        if (event.sensor.type == Sensor.TYPE_ACCELEROMETER) {
            val xForce = event.values[0]
            val yForce = event.values[1]
            val zForce = event.values[2]
            val shock = sqrt(xForce.pow(2) + yForce.pow(2) + zForce.pow(2));

            if (shock <= threshold && !isInFall) {
                fallStartTime = Calendar.getInstance().time
                isInFall = true
            } else if (shock > threshold && isInFall) {
                val lastFallStartTime = fallStartTime
                val fallEndTime = Calendar.getInstance().time
                isInFall = false
                showFallEndedNotification()
                launch {
                    fallAccess.insert(
                        FreeFall(
                            startTime = lastFallStartTime, endTime = fallEndTime,
                            shock = threshold
                        )
                    )
                }
            }
        }
    }

    private fun showFallEndedNotification() {
        val notification: Notification = NotificationCompat.Builder(
            this,
            CHANNEL_FALL_DETECTED
        )
            .setContentTitle(getString(R.string.fall_detected_notification_title))
            .setSmallIcon(R.drawable.ic_fall)
            .setAutoCancel(true)
            .build()

        val notificationId = (System.currentTimeMillis() and 0xfffffff).toInt()
        notificationManager.notify(notificationId, notification)
    }

}