package com.xbird.detectfreefall

import android.content.Context
import android.content.Intent
import android.hardware.Sensor
import android.hardware.SensorManager
import androidx.core.content.ContextCompat
import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import com.xbird.detectfreefall.access.FallAccess
import com.xbird.detectfreefall.database.AppDatabase
import com.xbird.detectfreefall.exceptions.AccelerometerUnavailableException
import com.xbird.detectfreefall.exceptions.NoInitException
import com.xbird.detectfreefall.model.FreeFall
import com.xbird.detectfreefall.service.DetectFreeFallService

object DetectFreeFall {

    private var context: Context? = null
    private var fallAccess: FallAccess? = null
    private var isRunning = MutableLiveData<Boolean>().apply { value = false }
    @Throws(AccelerometerUnavailableException::class)
    fun init(
        context: Context
    ) {
        this.context = context
        val sensorManager = this.context!!.getSystemService(Context.SENSOR_SERVICE)
                as SensorManager
        if (sensorManager.getDefaultSensor(Sensor.TYPE_ACCELEROMETER) == null) {
            throw AccelerometerUnavailableException()
        }
        this.fallAccess = AppDatabase.getDatabase(this.context!!).getFallAccess()
    }

    @Throws(NoInitException::class)
    fun startDetection(
        threshold: Double = DetectFreeFallService.DEFAULT_FREE_FALL_SHOCK,
        sensorDelay: Int = DetectFreeFallService.DEFAULT_SENSOR_DELAY
    ) {
        if (context == null) throw NoInitException()
        val serviceIntent = Intent(context, DetectFreeFallService::class.java)
            .apply {
                putExtra(DetectFreeFallService.EXTRA_KEY_SHOCK, threshold)
                putExtra(DetectFreeFallService.EXTRA_KEY_SENSOR_DELAY, sensorDelay)
            }
        ContextCompat.startForegroundService(context!!, serviceIntent)
    }

    @Throws(NoInitException::class)
    fun stopDetection() {
        if (context == null) throw NoInitException()
        val serviceIntent = Intent(context, DetectFreeFallService::class.java)
        context!!.stopService(serviceIntent)
    }

    @Throws(NoInitException::class)
    fun allFreeFalls(): LiveData<List<FreeFall>> {
        if (fallAccess == null) throw NoInitException()
        return fallAccess!!.allFreeFalls
    }

    fun getIsFallDetectionRunning(): LiveData<Boolean> = isRunning

    internal fun setIsFallDetectionRunning(isRunning: Boolean){
        this.isRunning .postValue(isRunning)
    }
}