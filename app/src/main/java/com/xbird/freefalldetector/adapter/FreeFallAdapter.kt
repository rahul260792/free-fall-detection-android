package com.xbird.freefalldetector.adapter

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.xbird.detectfreefall.model.FreeFall
import com.xbird.freefalldetector.databinding.FallItemBinding

class FreeFallAdapter : RecyclerView.Adapter<FreeFallAdapter.FreeFallViewHolder>() {

    var fallList: List<FreeFall> = emptyList()
        set(value) {
            field = value
            notifyDataSetChanged()
        }
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FreeFallViewHolder {
        val binding = FallItemBinding.inflate(LayoutInflater.from(parent.context), parent, false)
        return FreeFallViewHolder(binding)
    }

    override fun getItemCount(): Int {
        return fallList.size
    }

    override fun onBindViewHolder(holder: FreeFallViewHolder, position: Int) {
        holder.bind()
    }

    inner class FreeFallViewHolder(private val binding: FallItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind() {
            binding.freeFall = fallList[adapterPosition]
        }
    }
}