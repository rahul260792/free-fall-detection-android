package com.xbird.freefalldetector.viewModel

import android.app.Application
import androidx.lifecycle.AndroidViewModel
import com.xbird.detectfreefall.DetectFreeFall

class MainViewModel(application: Application) : AndroidViewModel(application) {

    init {
        DetectFreeFall.init(application)
    }

    val isDetectorRunning = DetectFreeFall.getIsFallDetectionRunning()

    val allFreeFall = DetectFreeFall.allFreeFalls()

    fun startDetection(){
        DetectFreeFall.startDetection(threshold = 1.5)
    }

    fun stopDetection(){
        DetectFreeFall.stopDetection()
    }
}